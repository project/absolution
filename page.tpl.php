<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
  </head>
 <body>
	<div id="container">
		<div id="header">
			<div id="logo">     
			  <?php if (!empty($logo)): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          <div class="site-title"><?php print $site_name ?></div>
          </a>
        <?php endif; ?>
      </div><!-- end of logo -->
			<div id="search"><?php print $search_box ?></div>
			<div id="secondary-nav">
        <?php if (!empty($primary_links)): ?>
          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
        <?php endif; ?>
			</div><!-- end of secondary-nav -->
		</div><!-- end of header -->
		<div id="top">
			<div id="primary-nav">
				<?php $menu_name = variable_get('menu_primary_links_source', 'primary-links');
print menu_tree($menu_name); ?>
			</div><!-- end of primary-nav -->
			<div id="slider"><img src="<?php print base_path() . path_to_theme() ?>/images/img.jpg" /></div>
		</div><!-- end of top -->
		<div id="leftside-bar"><?php print $leftside ?></div>
		<div id="content-area">
			<div id="left">
			  <?php print $left ?>
			</div><!-- end of left -->
			<div id="right">
        <?php print $right ?>
			</div><!-- end of right -->
			<div class="clear"></div>
			<div id="content">
			  <div class="tabs"><?php print $tabs ?><?php print $tabs2 ?></div>
			  <?php print $content ?>
			</div><!-- end of content -->
			<div class="clear"></div>
		</div><!-- end of content-area -->
		<div id="footer">
			<ul>
				<li><a href="#">Term of Use | </a></li>
				<li><a href="#">Privacy Policy</a></li>
			</ul>
			<?php print $footer_message ?>
		</div><!-- end of footer -->
	</div><!-- end of container -->
	<?php print $closure ?>
</body>
</html>
